import React, { Component } from "react";
import Slider from "./slider/Slider";
import Card from "./card/Card";

class Landing extends Component {
  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <Slider />
        <Card />
      </div>
    );
  }
}

export default Landing;
