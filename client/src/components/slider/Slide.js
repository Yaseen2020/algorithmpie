import React from "react";

class Slide extends React.Component {
  render() {
    let background = this.props.background;
    let text = this.props.text;
    let link = this.props.link;
    let active = this.props.active;

    let slideStyle = {
      backgroundImage: "url(" + background + ")"
    };

    return (
      <div className="slider__slide" data-active={active} style={slideStyle}>
        <div className="slider__slide__text">
          <a>{text}</a>
        </div>
      </div>
    );
  }
}

export default Slide;
