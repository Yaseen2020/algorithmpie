import React from "react";
import "./style.css";
import Slide from "./Slide";

class Slider extends React.Component {
  state = {
    activeSlide: 0,
    slides: [
      {
        background: "/images/Algorithm_pie_1.jpeg",
        text: "Imagine",
        link: "/"
      },
      {
        background: "/images/Algorithm_pie_2.jpeg",
        text: "Design",
        link: "/"
      },
      {
        background: "/images/Algorithm_pie_3.jpeg",
        text: "Create",
        link: "/"
      }
    ]
  };

  nextSlide = () => {
    let slide =
      this.state.activeSlide + 1 < this.state.slides.length
        ? this.state.activeSlide + 1
        : 0;
    this.setState({
      activeSlide: slide
    });
  };

  previousSlide = () => {
    let slide =
      this.state.activeSlide - 1 < 0
        ? this.state.slides.length - 1
        : this.state.activeSlide - 1;
    this.setState({
      activeSlide: slide
    });
  };

  render() {
    let slides = this.state.slides;
    console.log(slides);

    return (
      <div className="slider mobile">
        {slides.map((slide, index, array) => (
          <Slide
            background={slide.background}
            text={slide.text}
            active={index === this.state.activeSlide}
            link={slide.link}
          />
        ))}
        <div className="slider__next" onClick={this.nextSlide}>
          <i className="fa fa-4x fa-arrow-circle-right" />
        </div>
        <div className="slider__previous" onClick={this.previousSlide}>
          <i className="fa fa-4x fa-arrow-circle-left" />
        </div>
      </div>
    );
  }
}

export default Slider;
