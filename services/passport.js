const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const keys = require('../config/keys');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id)
    .then(user => done(null, user));
});

passport.use(new GoogleStrategy({
    clientID: keys.googleClientID,
    clientSecret: keys.googleClientSecret,
    callbackURL: '/auth/google/callback',
    proxy: true
}, (accessToken, refreshToken, profile, done)=> {
    User.findOne({googleId: profile.id})
    .then(exsistanceUser => {
        if(exsistanceUser){
            // We already have a user with this id.
            done(null, exsistanceUser);
        } else {
           // we dont any user with this id, please make a user with this id.
            new User({ googleId: profile.id}).save()
            .then(user => done(null, user));
        }
    })
    
}));

